<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Storage;
use Session;

class ProfilesController extends Controller
{

    public function index ($slug)
    {
        $user = User::where('slug',$slug)->firstOrFail();
        return view('profiles.profile', compact('user'));
    }

    public function edit()
    {
        return view('profiles.edit')->with('info', Auth::user()->profile);
    }

    public function update(Request $request)
    {

        $request->validate([
           'location' => 'required',
            'about' => 'required'
        ]);

       Auth::user()->profile()->update([
            'location' => $request->location,
            'about' => $request->about
        ]);

        if($request->hasFile('avatar'))
        {
            $oldAvatar = Auth::user()->avatar;
            Storage::delete($oldAvatar);
            Auth::user()->update([
               'avatar' => $request->avatar->store('public/avatars')
            ]);
        }

        $notification = array(
            'message' => 'Profile successfully updated.',
            'alert-type' => 'success'
        );

        return redirect()->route('profile', ['slug' => Auth::user()->slug])->with($notification);
    }
}
