<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class FriendshipsController extends Controller
{

    public function check($id)
    {
        if(Auth::user()->isFriendWith($id) === 1)
        {
            return ['status' => 'friends'];
        }

        else if(Auth::user()->hasPendingFriendRequestFrom($id) === 1)
        {
            return ['status' => 'pending'];
        }

        else if(Auth::user()->hasPendingFriendRequestSentTo($id) === 1)
        {
            return ['status' => 'waiting'];
        }

        return ['status' => 0];
    }

    public function addFriend($id)
    {
        //sending notifications, emails, broadcasting
        $response = Auth::user()->addFriend($id);
        User::find($id)->notify(new \App\Notifications\NewFriendRequest(Auth::user()));

        return $response;
    }

    public function acceptFriend($id)
    {
        //sending notifications, emails, broadcasting
        $response = Auth::user()->acceptFriend($id);
        User::find($id)->notify(new \App\Notifications\FriendRequestAccepted(Auth::user()));

        return $response;
    }
}
