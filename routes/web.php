<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function() {

    Route::get('/profile/edit/profile', [
        'uses' => 'ProfilesController@edit',
        'as' => 'profile.edit'
    ]);

    Route::post('/profile/update/profile', [
        'uses' => 'ProfilesController@update',
        'as' => 'profile.update'
    ]);

    Route::get('/profile/{slug}', [
      'uses' => 'ProfilesController@index',
       'as' => 'profile'
   ]);
   Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::get('/check-relationship-status/{id}', [
        'uses' => 'FriendshipsController@check',
        'as' => 'check'
    ]);

    Route::get('/add-friend/{id}', [
        'uses' => 'FriendshipsController@addFriend',
        'as' => 'addFriend'
    ]);

    Route::get('/accept-friend/{id}', [
        'uses' => 'FriendshipsController@acceptFriend',
        'as' => 'acceptFriend'
    ]);

    Route::get('/get-unread', function() {
       return Auth::user()->unreadNotifications;
    });

    Route::get('/notifications', [
        'uses' => 'HomeController@notifications',
        'as' => 'notifications'
    ]);

    Route::post('/create/post', [
        'uses' => 'PostsController@store',
        'as' => 'post.create'
    ]);

    Route::get('/feed', [
        'uses' => 'FeedsController@feed',
        'as' => 'feed'
    ]);

    Route::get('/get-auth-user-data', function () {
       return Auth::user();
    });

    Route::get('/like/{id}', [
        'uses' => 'LikesController@like'
    ]);

    Route::get('/unlike/{id}', [
        'uses' => 'LikesController@unlike'
    ]);

});