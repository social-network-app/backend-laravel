import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        nots: [],
        posts: [],
        authUser: {}
    },
    getters: {
        allNots(state) {
            return state.nots
        },
        allNotsCount(state) {
            return state.nots.length
        },
        allPosts(state) {
            return state.posts
        }
    },
    mutations: {
        ADD_NOT(state, not) {
            state.nots.push(not)
        },
        ADD_POST(state, post) {
            state.posts.push(post)
        },
        AUTH_USER_DATA(state, user) {
            state.authUser = user
        },
        UPDATE_POST_LIKES(state, payload) {
            let post = state.posts.find((p) => {
                return p.id === payload.id
            })
            post.likes.push(payload.like)
        },
        UNLIKE_POST(state, payload) {
            let post = state.posts.find((p) => {
                return p.id === payload.post_id
            })
            let like = post.likes.find((l) => {
                return l.id === payload.like_id
            })
            let index = post.likes.indexOf(like)
            post.likes.splice(index, 1)
        }

    },
    actions: {

    }
})